const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu extends cc.Component {
    

    onLoad(){
        cc.log("on load");
        let button = new cc.Component.EventHandler();
        button.target = this.node;
        button.component = "Menu";
        button.handler = "Button";
        cc.find("Canvas/UI/StartButton").getComponent(cc.Button).clickEvents.push(button);
    }
    // ===================== TODO =====================
    // 1. Add dynamic click event to StartButton to call this
    //    function
    loadGameScene(){
        cc.director.loadScene("game");
        cc.log("load scene game");

    }

    Button(event, customEventData){
        this.loadGameScene();
    }
    // ================================================
}
